module Signzoneinc
  module Api
    class Client
      module Invoice

        def get_invoice(signZoneCustomerNumber, invoiceNumber)
          conn = @client.get do |req|
            req.url "v2/Invoice?signZoneCustomerNumber=#{signZoneCustomerNumber}&invoiceNumber=#{invoiceNumber}"
          end
          conn.body
        end

        def list_invoices(signZoneCustomerNumber)
          conn = @client.get do |req|
            req.url "v2/Invoice/New?signZoneCustomerNumber=#{signZoneCustomerNumber}"
          end
          conn.body
        end

        def mark_invoices_received(signZoneCustomerNumber, json_payload)
          conn = @client.patch do |req|
            req.url "v2/Invoice/Received?signZoneCustomerNumber=#{signZoneCustomerNumber}"
            req.body = json_payload
          end
          conn.body
        end

      end
    end
  end
end
