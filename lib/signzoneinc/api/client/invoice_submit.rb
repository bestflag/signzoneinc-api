module Signzoneinc
  module Api
    class Client
      module InvoiceSubmit

        def submit_invoice(signZoneCustomerNumber)
          conn = @client.get do |req|
            req.url "v1/InvoiceSubmit?signZoneCustomerNumber=#{signZoneCustomerNumber}"
          end
          conn.body
        end

      end
    end
  end
end

