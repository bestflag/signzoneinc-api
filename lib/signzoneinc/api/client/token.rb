module Signzoneinc
  module Api
    class Client
      module Token

        def authentication(username, password)
          conn = @client.post do |req|
            req.url "v2/Token?username=#{username}&password=#{password}"
          end
          @client.authorization :Bearer, "#{conn.body["access_token"]}"
        end

        def manual_override(token)
          @client.authorization :Bearer, "#{token}"
        end

      end
    end
  end
end
