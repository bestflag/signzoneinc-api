module Signzoneinc
  module Api
    class Client
      module Order

        def create_order(json_payload)
          conn = @client.post do |req|
            req.url 'v2/Order'
            req.headers['Content-Type'] = 'application/json'
            req.body = json_payload
          end
          conn.body
        end

        def get_order_status(orderNumber)
          conn = @client.get do |req|
            req.url "v1/Order/#{orderNumber}"
          end
          conn.body
        end

      end
    end
  end
end
