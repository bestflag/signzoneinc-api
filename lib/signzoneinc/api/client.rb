require_relative '../api/client/invoice'
require_relative '../api/client/invoice_submit'
require_relative '../api/client/order'
require_relative '../api/client/token'

require 'faraday'
require 'faraday_middleware'

module Signzoneinc
  module Api
    class Client
      def initialize()
        @client ||= Faraday.new(url: 'https://webapi.signzoneinc.com') do |http|
          http.request :multipart
          http.request :url_encoded
          http.adapter :net_http
          http.response :json, content_type: /\bjson$/
        end
      end

      include Invoice
      include InvoiceSubmit
      include Order
      include Token
    end
  end
end
