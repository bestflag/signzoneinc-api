require "signzoneinc/api/version"
require "signzoneinc/api/client"

module Signzoneinc
  module Api
    class Error < StandardError; end

    def new()
      Signzoneinc::Api::Client.new
    end
  end
end
